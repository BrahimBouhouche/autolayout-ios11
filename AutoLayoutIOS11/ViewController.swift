//
//  ViewController.swift
//  AutoLayoutIOS11
//
//  Created by mac on 9/27/18.
//  Copyright © 2018 Brahim Bouhouche. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let square = UIView(frame: CGRect(x: self.view.frame.width/2 , y: self.view.frame.height/2, width: 100, height: 100))
        square.backgroundColor = UIColor.red
        self.view.addSubview(square)
    }


}

